﻿using Example2.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Example2.DAL
{
    public class UserContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }
}