﻿using Example2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Example2.DAL
{
    public class UserCollection
    {
        public List<User> theUsers;

        public UserCollection()
        {
            theUsers = new List<User>
            {
                new User { FirstName = "John", LastName = "Doe", DOB = new DateTime(1890,10,31) }
            };
        }
    }
}