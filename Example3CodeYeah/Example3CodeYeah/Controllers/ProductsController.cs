﻿using Example3CodeYeah.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using Example3CodeYeah.ViewModels;

namespace Example3CodeYeah.Controllers
{
    public class ProductsController : Controller
    {
        private NorthWindContext db = new NorthWindContext();

        // GET: Products
        public ActionResult Index()
        {
            var products = db.Products.OrderByDescending(p => p.UnitPrice).ToList();
            var suppliers = db.Suppliers.ToList();
            MainViewModel vm = new MainViewModel { TheProducts = products, TheSuppliers = suppliers };
            //return View(products);
            return View(vm);
        }

        public ActionResult Find(int? id = 1)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = db.Products.Find(id);
            if(product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

    }
}