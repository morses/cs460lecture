﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Example3CodeYeah.Controllers
{
    public class AJAXExampleController : Controller
    {
        // GET: AJAXExample
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Gimme(int? id = 100)
        {
            Random gen = new Random();
            int[] arr = new int[(int)id];
            for(int i = 0; i < arr.Length; ++i)
            {
                arr[i] = gen.Next(1000);
            }

            var data = new { Message = "Hello from controller action method",
                             num = (int)id,
                             scores = arr
                           };

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}