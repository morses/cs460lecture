﻿// do an ajax call to get some numbers
$("#request").click(function () {
    var a = $("#Number").val();
    console.log(a);
    var source = "/AJAXExample/Gimme/" + a;
    console.log(source);
    // get data in JSON format from our controller
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: displayData
    });
});

function displayData(data) {
    $("#Count").text("Number of values requested: " + data.num);
    $("#Values").text("The values: " + data.scores);
    var sum = data.scores.reduce(function (a, b) { return a + b; });
    var ave = sum / data.scores.length;
    $("#Average").text("Average of values is: " + ave);
}