﻿using Example3CodeYeah.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Example3CodeYeah.ViewModels
{
    public class MainViewModel
    {
        public List<Product> TheProducts { get; set; }
        public List<Supplier> TheSuppliers { get; set; }
    }
}