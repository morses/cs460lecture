﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreakDependencies.Models.Virtual
{
    public class Schedule
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Color Color { get; set; }
    }
}