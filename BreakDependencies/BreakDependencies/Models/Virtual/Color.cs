﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreakDependencies.Models.Virtual
{
    public class Color
    {
        public string ColorValue { get; set; }
        public string ColorName { get; set; }
    }
}