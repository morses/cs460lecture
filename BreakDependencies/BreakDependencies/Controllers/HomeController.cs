﻿using BreakDependencies.Abstract;
using BreakDependencies.Models.Virtual;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BreakDependencies.Controllers
{
    public class HomeController : Controller
    {
        IScheduleRepository scheduleRepository;

        public HomeController(IScheduleRepository repo)
        {
            scheduleRepository = repo;
        }

        // GET: Home
        public ActionResult Index()
        {
            // Get all the schedules
            IEnumerable<Schedule> skeds = scheduleRepository.GetAllSchedules();
            return View(skeds);
        }
    }
}