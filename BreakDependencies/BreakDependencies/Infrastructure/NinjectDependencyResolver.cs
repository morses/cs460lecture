﻿using BreakDependencies.Abstract;
using BreakDependencies.Models.Virtual;
using Moq;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BreakDependencies.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            // Tell NInject what to use to meet a dependency
            Mock<IScheduleRepository> mock = new Mock<IScheduleRepository>();
            mock.Setup(m => m.GetAllSchedules()).Returns( new List<Schedule> {
                new Schedule { Name="Some Name", Description="Heres a description", Color = new Color { ColorValue="#AABBCC" } },
                new Schedule { Name="adfad", Description="xxxxxxxxxxxxxx", Color = new Color { ColorValue="#FFFFFF" } },
                new Schedule { Name="adfnnnnnnnnnnnad", Description="aaaaaaaaaaa", Color = new Color { ColorValue="#006677" } }
                });
            kernel.Bind<IScheduleRepository>().ToConstant(mock.Object);
        }
    }
}