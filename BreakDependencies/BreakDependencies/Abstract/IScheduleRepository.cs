﻿using BreakDependencies.Models.Virtual;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreakDependencies.Abstract
{
    public interface IScheduleRepository
    {
        IEnumerable<Schedule> GetAllSchedules();
    }
}