﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using AuthWithIdentity.Models;

namespace AuthWithIdentity.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationUserManager _userManager;

        public ActionResult Index()
        {
            Debug.WriteLine(User.Identity.GetUserId());
            Debug.WriteLine(User.Identity.IsAuthenticated);
            Debug.WriteLine(User.Identity.GetUserName());

            if (User.Identity.IsAuthenticated)
            {
                ApplicationUser currentUser = UserManager.FindById(User.Identity.GetUserId());
                ViewBag.Pseudonym = currentUser.Pseudonym;
            }
            else
                ViewBag.Pseudonym = "";
            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}