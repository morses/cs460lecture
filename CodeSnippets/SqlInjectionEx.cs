using Final.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Final.Controllers
{
    public class HomeController : Controller
    {
        private FinalDBContext db = new FinalDBContext();

        // GET: Home
        public ActionResult Index()
        {
            return View(db.Genres.ToList());
        }

        public JsonResult GenreInfo(string genre)
        {
            var resultArray = db.Database.SqlQuery<ArtWork>("select aw.Title, aw.Artist, aw.AWID from Classification c join ArtWork aw on c.ArtWork=aw.Title where genre="+"'"+genre+"'").ToArray();
            var results = new { array = resultArray, size = resultArray.Length };
            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }
}

// See: "What is SQL Injection Attack", by kudvenkat
//      https://www.youtube.com/watch?v=uSw0IoSr3Hk









// From: https://msdn.microsoft.com/en-us/library/system.data.entity.database(v=vs.113).aspx

/*
System.Data.Entity.Database.SqlQuery<TElement>(String, Object[]) --

Creates a raw SQL query that will return elements of the given generic type. The type can be
any type that has properties that match the names of the columns returned from the query, or
can be a simple primitive type. The type does not have to be an entity type. The results of
this query are never tracked by the context even if the type of object returned is an entity
type. Use the SqlQuery method to return entities that are tracked by the context. As with
any API that accepts SQL it is important to parameterize any user input to protect against
a SQL injection attack. You can include parameter place holders in the SQL query string and
then supply parameter values as additional arguments. Any parameter values you supply will
automatically be converted to a DbParameter.
context.Database.SqlQuery<Post>("SELECT * FROM dbo.Posts WHERE Author = @p0", userSuppliedAuthor);
Alternatively, you can also construct a DbParameter and supply it to SqlQuery.
This allows you to use named parameters in the SQL query string.
context.Database.SqlQuery<Post>("SELECT * FROM dbo.Posts WHERE Author = @author", new SqlParameter("@author", userSuppliedAuthor));
*/

    // Compare with LINQ version
public JsonResult GetArtworksByGenre(int id)
{
    // !! No error checking here on id
    using (ArtWorksContext db = new ArtWorksContext())
    {
        var artworks = db.Genres.Find(id)
                       .Classifications
                       .Select(c => new
                       {
                           Title = c.ArtWork.Title,
                           Author = c.ArtWork.Artist.Name
                       }).ToList();
        return Json(artworks, JsonRequestBehavior.AllowGet);
    }
}